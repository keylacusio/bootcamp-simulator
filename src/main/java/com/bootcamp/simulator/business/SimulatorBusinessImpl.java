package com.bootcamp.simulator.business;

import com.bootcamp.simulator.model.Constants;
import com.bootcamp.simulator.model.entities.SimulatorEntity;
import com.bootcamp.simulator.model.entities.UserEntity;
import com.bootcamp.simulator.model.expose.SimulatorRq;
import com.bootcamp.simulator.model.expose.SimulatorRs;
import com.bootcamp.simulator.repository.SimulatorRepositoryJpa;
import com.bootcamp.simulator.repository.UserRepositoryJpa;
import io.reactivex.Maybe;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.util.Optional;
import java.util.function.Function;

@Service
@RequiredArgsConstructor
public class SimulatorBusinessImpl implements SimulatorBusiness {

    private final SimulatorRepositoryJpa simulatorRepositoryJpa;
    private final UserRepositoryJpa userRepositoryJpa;


    @Override
    public Maybe<SimulatorRs> executeSimulation(SimulatorRq simulatorRq) {

        Optional<UserEntity> byDocumentNumber =
                userRepositoryJpa.findByDocumentNumber(simulatorRq.getDni());
        return  (byDocumentNumber.isPresent() ?
                Maybe.just(byDocumentNumber) : Maybe.empty())
                .switchIfEmpty(Maybe.error(new Exception("No se encontró al usuario")))
                .map(req -> (Math.pow(simulatorRq.getTea() / 100 + 1, 30.0 / 360.0) - 1) * 100)
                .map(monthlyEffectiveRate -> simulatorRepositoryJpa.save(SimulatorEntity.builder()
                        .cardType(simulatorRq.getCard())
                        .currencyType(simulatorRq.getCurrency())
                        .amount(simulatorRq.getAmount())
                        .feePayment(simulatorRq.getFeePayment())
                        .tea(simulatorRq.getTea())
                        .paymentDate(LocalDate.of(
                                LocalDate.now().getYear(),
                                LocalDate.now().getMonth().getValue(),
                                simulatorRq.getPaymentDay()))
                        .firstFeePayment(simulatorRq.getAmount()
                                .multiply(BigDecimal.valueOf(monthlyEffectiveRate / 100 * (Math.pow((1 + monthlyEffectiveRate / 100), simulatorRq.getFeePayment())) /
                                        (Math.pow((1 + monthlyEffectiveRate / 100), simulatorRq.getFeePayment()) - 1))))
                        .status(Constants.SUCCESSFUL)
                        .build()))
                .doOnError(err -> SimulatorRs.builder().status(Constants.ERROR).build())
                .map(mapEntityToRs::apply);
    }

    private final Function<SimulatorEntity, SimulatorRs> mapEntityToRs =
            (entity) -> SimulatorRs.builder()
                    .amount(entity.getAmount().setScale(2, RoundingMode.HALF_UP))
                    .currency(entity.getCurrencyType())
                    .firstFeePayment(entity.getFirstFeePayment().setScale(2, RoundingMode.HALF_UP))
                    .paymentDate(entity.getPaymentDate())
                    .status(entity.getStatus())
                    .build();

}
