package com.bootcamp.simulator.business;

import com.bootcamp.simulator.model.expose.SimulatorRq;
import com.bootcamp.simulator.model.expose.SimulatorRs;
import com.bootcamp.simulator.repository.SimulatorRepositoryJpa;
import io.reactivex.Maybe;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
public interface SimulatorBusiness {

    Maybe<SimulatorRs> executeSimulation(SimulatorRq simulatorRq);

}
