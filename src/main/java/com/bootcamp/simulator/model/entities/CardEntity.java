package com.bootcamp.simulator.model.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Table(name = "CARD")
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CardEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "CARD_ID")
    private Long cardId;

    @Column(name = "CARD_NUMBER")
    private String cardNumber;

    @Column(name = "CARD_TYPE")
    private String cardType;

    @ManyToOne
    @JoinColumn(name="user_id", referencedColumnName="USER_ID")
    private UserEntity user;

}
