package com.bootcamp.simulator.model.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;

@Data
@Table(name = "SIMULATOR")
@Entity
@Builder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
public class SimulatorEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long simulatorId;
    private String cardType;
    private String currencyType;
    private BigDecimal amount;
    private Integer feePayment;
    private Double tea;
    private LocalDate paymentDate;
    private BigDecimal firstFeePayment;
    private String status;
    //private UserEntity user;
}
