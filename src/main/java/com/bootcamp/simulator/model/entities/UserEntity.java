package com.bootcamp.simulator.model.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Data
@Table(name = "USER")
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "USER_ID")
    private Long userId;

    @Column(name = "DOCUMENT_NUMER")
    private String documentNumber;

    @Column(name = "DOCUMENT_TYPE")
    private String documentType;

    @Column(name = "NAME")
    private String name;

    @Column(name = "LASTNAME")
    private String lastname;

    @OneToMany(mappedBy = "user", cascade = CascadeType.MERGE)
    private List<CardEntity> cards = new ArrayList<>();
}
