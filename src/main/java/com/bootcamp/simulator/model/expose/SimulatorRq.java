package com.bootcamp.simulator.model.expose;

import com.bootcamp.simulator.util.validator.CardTypeValidation;
import com.bootcamp.simulator.util.validator.PaymentDayValidation;
import com.bootcamp.simulator.util.validator.TeaValidation;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.*;

import java.math.BigDecimal;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SimulatorRq {

    @NotEmpty(message = "Dni vacío")
    private String dni;

    @CardTypeValidation(message = "Tarjeta inválida, puede ser CLASSIC, GOLD o BLACK")
    private String card;

    @NotNull
    private BigDecimal amount;

    @NotEmpty
    private String currency;

    @Range(min = 1, max = 36)
    private Integer feePayment;

    @TeaValidation(message = "Tea Inválido, puede ser 99.9, 95.9 o 90.90")
    private Double tea;

    @PaymentDayValidation(message = "Dia de pago Inválido, puede ser 5 o 20")
    private Integer paymentDay;
}
