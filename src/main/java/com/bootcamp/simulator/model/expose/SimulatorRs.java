package com.bootcamp.simulator.model.expose;

import lombok.*;

import javax.validation.constraints.NotEmpty;
import java.math.BigDecimal;
import java.time.LocalDate;

@Data
@Builder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
public class SimulatorRs {
    private BigDecimal firstFeePayment;
    private BigDecimal amount;
    private String currency;
    private LocalDate paymentDate;
    private String status;
}
