package com.bootcamp.simulator.model.expose;

import lombok.Builder;
import lombok.Data;
import org.apache.tomcat.jni.Local;
import org.springframework.http.HttpStatus;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Data
public class ErrorResponse {
    private LocalDateTime timestamp;
    private HttpStatus status;
    private List<Map<String, String>> errors;

    public ErrorResponse(HttpStatus status, List<Map<String, String>> errors, LocalDateTime timestamp) {
        super();
        this.status = status;
        this.errors = errors;
        this.timestamp = timestamp;
    }
}
