package com.bootcamp.simulator.config;

import com.bootcamp.simulator.model.entities.CardEntity;
import com.bootcamp.simulator.model.entities.UserEntity;
import com.bootcamp.simulator.repository.CardRepositoryJpa;
import com.bootcamp.simulator.repository.UserRepositoryJpa;
import lombok.AllArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Arrays;
import java.util.Collections;

@Configuration
@AllArgsConstructor
public class InitialDataBean {

    private final UserRepositoryJpa userRepositoryJpa;
    private final CardRepositoryJpa cardRepositoryJpa;

    @Bean
    public void initDatabase() {
        userRepositoryJpa.saveAll(Arrays.asList(
                UserEntity.builder()
                        .documentNumber("72787489")
                        .documentType("1")
                        .name("Adriana")
                        .lastname("Lopez")
                        .cards(Arrays.asList(
                                CardEntity.builder()
                                        .cardId(1L)
                                        .cardNumber("4758-9475-8374-9388")
                                        .cardType("BLACK")
                                        .build(),
                                CardEntity.builder()
                                        .cardId(2L)
                                        .cardNumber("7384-2837-9384-1234")
                                        .cardType("CLASSIC")
                                        .build()))
                        .build(),
                UserEntity.builder()
                        .documentNumber("55283481")
                        .documentType("1")
                        .name("Luis")
                        .lastname("Contreras")
                        .cards(Collections.singletonList(
                            CardEntity.builder()
                                .cardId(3L)
                                .cardNumber("3747-8374-2736-8473")
                                .cardType("GOLD")
                                .build()))
                        .build()
        ));

        cardRepositoryJpa.saveAll(Arrays.asList(
                CardEntity.builder()
                        .cardId(1L)
                        .cardNumber("3747-8374-2736-8473")
                        .cardType("GOLD")
                        .user(UserEntity.builder().userId(1L).build())
                        .build(),
                CardEntity.builder()
                        .cardId(2L)
                        .cardNumber("4758-9475-8374-9388")
                        .cardType("BLACK")
                        .user(UserEntity.builder().userId(1L).build())
                        .build(),
                CardEntity.builder()
                        .cardId(3L)
                        .cardNumber("7384-2837-9384-1234")
                        .cardType("CLASSIC")
                        .user(UserEntity.builder().userId(2L).build())
                        .build()
        ));

    }
}
