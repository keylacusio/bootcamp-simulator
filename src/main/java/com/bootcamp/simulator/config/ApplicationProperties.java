package com.bootcamp.simulator.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Data
@ConfigurationProperties(prefix = "app.constants")
public class ApplicationProperties {
    private List<Card> cardTypes;
    private List<PaymentDay> paymentDays;
    private List<Rate> tea;

    @Data
    public static class Card {
        private String code;
        private String name;
        private String description;
    }

    @Data
    public static class PaymentDay {
        private Integer number;
        private String description;
    }

    @Data
    public static class Rate {
        private Double rate;
    }
}
