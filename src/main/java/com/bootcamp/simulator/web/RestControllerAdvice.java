package com.bootcamp.simulator.web;

import com.bootcamp.simulator.model.expose.ErrorResponse;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.springframework.http.HttpStatus.BAD_REQUEST;

@org.springframework.web.bind.annotation.RestControllerAdvice
public class RestControllerAdvice extends ResponseEntityExceptionHandler {

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(
            MethodArgumentNotValidException ex,
            HttpHeaders headers,
            HttpStatus status,
            WebRequest request) {
        List<Map<String, String>> errors = new ArrayList<>();
        Map<String, String> errorMap;
        for (FieldError error : ex.getBindingResult().getFieldErrors()) {
            errorMap = new HashMap<>();
            errorMap.put("field", error.getField());
            errorMap.put("message", error.getDefaultMessage());
            errors.add(errorMap);
        }
        for (ObjectError error : ex.getBindingResult().getGlobalErrors()) {
            errorMap = new HashMap<>();
            errorMap.put("field", error.getObjectName());
            errorMap.put("message", error.getDefaultMessage());
            errors.add(errorMap);
        }

        ErrorResponse apiError =
                new ErrorResponse(HttpStatus.BAD_REQUEST, errors, LocalDateTime.now());
        return handleExceptionInternal(
                ex, apiError, headers, apiError.getStatus(), request);
    }

}
