package com.bootcamp.simulator.web;

import com.bootcamp.simulator.business.SimulatorBusiness;
import com.bootcamp.simulator.model.expose.SimulatorRq;
import com.bootcamp.simulator.model.expose.SimulatorRs;
import io.reactivex.Maybe;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
@RequestMapping("simulator")
public class SimulatorController {

    private final SimulatorBusiness simulatorBusiness;

    @PostMapping
    public Maybe<SimulatorRs> save(@Valid @RequestBody SimulatorRq simulatorRq){
        return simulatorBusiness.executeSimulation(simulatorRq);
    }

}
