package com.bootcamp.simulator.repository;

import com.bootcamp.simulator.model.entities.CardEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CardRepositoryJpa extends JpaRepository<CardEntity, Long> {

}
