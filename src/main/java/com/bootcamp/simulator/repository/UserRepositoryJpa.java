package com.bootcamp.simulator.repository;

import com.bootcamp.simulator.model.entities.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserRepositoryJpa extends JpaRepository<UserEntity, Long> {

    Optional<UserEntity> findByDocumentNumber(String documentNumber);
}
