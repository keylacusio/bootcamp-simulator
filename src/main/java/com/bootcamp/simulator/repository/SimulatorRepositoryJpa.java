package com.bootcamp.simulator.repository;

import com.bootcamp.simulator.model.entities.SimulatorEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SimulatorRepositoryJpa extends JpaRepository<SimulatorEntity, Long> {
}
