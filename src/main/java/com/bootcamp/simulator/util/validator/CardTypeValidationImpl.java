package com.bootcamp.simulator.util.validator;

import com.bootcamp.simulator.config.ApplicationProperties;
import lombok.AllArgsConstructor;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

@AllArgsConstructor
public class CardTypeValidationImpl implements ConstraintValidator<CardTypeValidation, String> {

    private final ApplicationProperties applicationProperties;

    @Override
    public boolean isValid(String cardType, ConstraintValidatorContext constraintValidatorContext) {
        return applicationProperties.getCardTypes()
                .stream()
                    .anyMatch(cardTypeItem -> cardTypeItem.getCode().equalsIgnoreCase(cardType));
    }
}
