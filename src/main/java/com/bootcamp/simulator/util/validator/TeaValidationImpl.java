package com.bootcamp.simulator.util.validator;

import com.bootcamp.simulator.config.ApplicationProperties;
import lombok.AllArgsConstructor;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Objects;

@AllArgsConstructor
public class TeaValidationImpl implements ConstraintValidator<TeaValidation, Double> {

    private final ApplicationProperties applicationProperties;

    @Override
    public boolean isValid(Double tea, ConstraintValidatorContext constraintValidatorContext) {
        return Objects.nonNull(tea) && applicationProperties.getTea()
                .stream()
                    .anyMatch(teaProperties -> teaProperties.getRate().equals(tea));
    }
}
