package com.bootcamp.simulator.util.validator;

import com.bootcamp.simulator.config.ApplicationProperties;
import lombok.AllArgsConstructor;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Objects;

@AllArgsConstructor
public class PaymentDayValidationImpl implements ConstraintValidator<PaymentDayValidation, Integer> {

    private final ApplicationProperties applicationProperties;

    @Override
    public boolean isValid(Integer paymentDay, ConstraintValidatorContext constraintValidatorContext) {
        return Objects.nonNull(paymentDay) && applicationProperties.getPaymentDays()
                .stream()
                .anyMatch(paymentDayProperties -> paymentDay.equals(paymentDayProperties.getNumber()));
    }
}
