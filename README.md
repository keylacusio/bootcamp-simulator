Levantar la aplicación:

Base de datos:

- La aplicación utiliza base de datos en memoria H2 por lo 
  que se podrá ver la base de datos al levantar la aplicación en 
  la siguiente ruta:
  http://localhost:8085/h2
  
- Loguearse con la siguiente información:
  * JDBC URL: jdbc:h2:mem:testdb
  * User Name: sa
  * Password: 
    
Endpoinds:

- POST http://localhost:8085/simulator

Request de ejemplo:

```json
{
"dni": "72787489",
"card": "GOLD",
"amount": 4000,
"currency": "PEN",
"feePayment": 12,
"tea": 90.90,
"paymentDay": 20
}
```

  

  